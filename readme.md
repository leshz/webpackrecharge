# Migración de Gulp a Webpack 👾

La siguiente es una guia sobre como implementa _Webpack_, sus ventajas y mejora nuestro trabajo en frontend.

- [Que es Webpack](#que-es-webpack)
- [Instalacion](#instalacion)
- [Archivos de configuracion](#archivos-de-configuración-)
- [Agregando Scripts](#agregando-scripts-)
- [Informacion de modulos ES6](#modulos-es6)
- [Estructura de carpetas y archivos](#estructura-de-archivos)

# Que es Webpack 

_Webpack_ es una aplicacion basada en [_NodeJS_](https://nodejs.org/es/) encargada manipular los archivos necesarios para frontent _(js|css|imagenes|fuentes|etc)_, transpilar Javascript , controlar las dependiencias y entregar los archivos listos para produccion, con esta descripcion aparentemente no existe mucha diferencia con _Gulp_, sin embargo _Webpack_ podria considerarse como la evolucion de _Gulp_ con superpoderes.

## Que ventajas trae este cambio 🤯

- Manejo de dependencias
- Eficiencia en carga de Javascript
- Código modular 
- Facilita las pruebas unitarias
- Análisis de los archivos finales
- Facilita la carga de dependencias asíncronas
- Conversión de formatos y compresion de _assets_
- Mayor comodidad para el desarrollador
- Gran comunidad

Más información en [pagina oficial de webpack](https://webpack.js.org).

## Que es ES6 

También se conoce como ECMAScript 2015, es la versión estandar de Javascript aprobada en Junio 2015, esta version contiene mejoras que se usaran en esta guia, principalmente el uso de modulos.

---

# Instalación 👾

## Antes de empezar 👌🏻

Antes de empezar a usar _Webpack_ se necesita tener instalado en el equipo [NodeJS](https://nodejs.org/es/) y su sistema de gestión de paquetes [NPM](https://www.npmjs.com).

En esta guía se asume que ya existe un archivo llamado [package.json](https://docs.npmjs.com/files/package.json) en el path raiz del *modulo / proyecto*, de no ser asi NPM tiene un comando para crea el archivo dentro de el directorio.

```shell 
npm init -y
``` 
## Instalacion de dependencias

Luego de tener _NodeJs_ y _NPM_ instalado en el quipo, el siguiente paso es instalar las dependencias para mantener el flujo de trabajo que llevamos con _Gulp_.

Primero se instala [_Babel_](https://babeljs.io/), esta dependencia permite programar con las ultimas versiones de Javascript sin problemas del soporte en los diferentes navegadores.

```shell
npm i -D babel babel-cli @babel/core babel-loader @babel/preset-env 
```

Despues se instala lo necesario para procesar lenguaje [SCSS](https://sass-lang.com/guide) para transformarlo en archivos CSS.

```shell
npm i -D node-sass sass-loader
```

Ahora se instalan dependencias que permiten a _Webpack_ manipilar los archivos CSS.

```shell
npm i -D css-hot-loader css-loader style-loader mini-css-extract-plugin  
```

Para extender más la funcionalidades de CSS instalamos la herramienta [postCSS](https://postcss.org/) 

```shell 
npm i -D postcss-import postcss-load-config postcss-loader postcss-preset-env cssnano
```

Por ultimo se instala _Webpack_ en su **version 4**

```shell
npm i -D path webpack webpack-cli webpack-dev-server
```
---

# Archivos de configuración 📖

Ahora que tenemos todo lo necesario instalado en nuestro equipo, es necesario configurar _Webpack_, indicarle a la aplicacion que necesitamos hacer con los archivos, en que y como transformarlos y en donde serán ubicados una vez termine el proceso. Para esto existe el archivo de configuracion **webpack.config.js**.

Este archivo debe estar en el directorio raiz del modulo o proyecto y se exporta un modulo de _NodeJS_ con el objeto de configuracion.

```javascript
module.exports = {

// Dentro de este objeto exportamos la configuracion

}
```
## Entries y Output

La configuracion basica consiste en dos elementos, **Entry** y **Output**, con estos elementos indicamos cual es nuestro archivo principal o punto de entrada de la aplicacion y en que directorio ubica el archivo final y su nombre.

```javascript
module.exports = {
  entry: './path/to/my/entry/file.js',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'main.bundle.js'
  }
};
```

_Una de las ventajas de Webpack es la posibilidad de manejar multiples puntos de entrada, se definen en la configuracion de esta manera._

```javascript
  entry: {
    example_name_1: "./path/to/my/entry/file_1.js"),
    example_name_2: "./path/to/my/entry/file_2.js"),
    example_name_3: "./path/to/my/entry/file_3.js")
  }
```

## Loader y Plugins

_Webpack_ solo entiende Javascript, por tal motivo, existen los loaders y los plugins, estos se añaden al archivo de configuracion ampliando las posibilidades para el desarrollador.

## Loaders

Los _loaders_ se configuran dentro de un objeto llamado **rules** y esta compuesto por dos partes principales.
- **TEST** : Sirve para indicar que tipo de archivos y extensiones van a ser procesados por ese loader.
- **USE** : Sirve para indicar que loader es el encargado de esa transformacion.

```javascript
module: {
  rules: [
    { test: /\.txt$/, use: 'raw-loader' }
  ]
}
```
## Plugins

A diferencia de los loaders que solo permiten transformaciones de archivos los plugins ofrecen una mayor gama de posibilidades, como optimizacion del empaquetado, inyeccion de assets o definicion de variables de entorno.

En este caso de ejemplo se usa [_HtmlWebpackPlugin_](https://github.com/jantimon/html-webpack-plugin) que simplifica la creacion de documentos HTML, similar a un sistema de plantillas.

```javascript
const HtmlWebpackPlugin = require('html-webpack-plugin');
const webpack = require('webpack');

module.exports = {
  plugins: [
    new HtmlWebpackPlugin({template: './src/index.html'})
  ]
};

```

## _Webpack.config.js_

La configuracion de _Webpack_ puede tener diferentes formas dependiendo de las tecnologias, plugins y loaders se esten usando y cada uno puede aceptar cierto tipo de parametros dentro de la misma configuracion, por lo que aqui se propone una configuracion estandar basada en las funcionalidades ya establecidas en _Gulp_.

**este archivo esta sujeto a cambios**

```javascript
const path = require("path");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

const buildPath = "path del modulo";

module.exports = {
  mode: "production",
  entry: {
    main: path.resolve(__dirname, "src/core/index.js")
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader",
          options: {
            presets: ["@babel/preset-env"]
          }
        }
      },
      {
        test: /\.(scss|css)$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader
          },
          {
            loader: "css-loader"
          },
          {
            loader: "postcss-loader",
            options: {
              ident: "postcss"
            }
          },
          {
            loader: "sass-loader"
          }
        ]
      }
    ]
  },
  resolve: {
    extensions: ["*", ".js", ".jsx"]
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: "/css/[name]-main.css"
    })
  ],
  output: {
    path: path.resolve(__dirname, "dist"),
    publicPath: buildPath,
    filename: "js/[name]-main.js"
  }
};

```

si desean ampliar estos conceptos puede ingresar [Aqui](https://webpack.js.org/concepts)

---
## Configuración PostCSS

Dentro del flujo de trabajo incluimos el loader de [PostCSS](https://postcss.org/), este loader necesita tambien su propio archivo de configuracion donde incluimos ciertos parametros para la transformacion del codigo de CSS.

**este archivo esta sujeto a cambios**

```javascript
module.exports = {
  plugins: {
    "postcss-import": {},
    "postcss-preset-env": {
      browsers: "last 2 versions"
    },
    cssnano: {}
  }
};
```
---

# Agregando scripts 💥

Luego del proceso configuracion vamos a incluir los siguientes comandos dentro del archivo [package.json](https://docs.npmjs.com/files/package.json) esto con el fin de ejecutar las tareas de una forma más comoda.

```json
  "scripts": {
    "build": "webpack -p --config ./webpack.config.js --display-error-details",
    "watch": "webpack -d --watch --config ./webpack.config.js --display-error-details"
  }
```
Estos script tienen una funcionalidad especifica: 

  - **build** : Crea el _bundle_ o archivo final para producción.
  - **watch** : _Webpack_ está pendiende de los cambios en los archivos para crear nuevamente el _bundle_ o archivo final para desarrollo.
---
# Modulos ES6

 Los modulos ES6 permite la importación y exportación de código entre diferentes archivos Javascript, permitiendo trabajar de forma más flexible el código Javascript.

## Exportación

Con la palabra **export** se exporta uno o varios elemento del archivo actual, es posible exportar variables, funciones, clases, declaraciones etc.

Existes varias formas de crear una exportacion de modulos, en esta guia se usarán las principales.

_En este caso declaramos una constante y la exportamos_

```javascript
// file : key.js

const key = "27hd7236" ; 

export {key};
```

_Tambien es posible una exportacion de multiples elementos_

```javascript
// file : values.js

const key = "3987987985465";
const user = "trf44678";

export { key , user }

```

_... o directamente en la declaracion del elemento_

```javascript
// file : functions.js

export function getKey(data) {
  return data
}

export getUser(data) {
  return data
}
```
_Cuando exportamos por default, sólo puede haber una exportación por archivo._

```javascript
// file : class.js

export default class User {
  constructor(name){
    this.name =name;
  }
  getName(){
    return this.name;
  }
}
```

Si desea ampliar la informacion acerca de _export_ puede ingresar [aqui](https://developer.mozilla.org/es/docs/Web/JavaScript/Referencia/Sentencias/export)


## Importación
Con palabra **import** se puede importar uno o varios elementos exportados en otro archivo Javascript.

Existes varias formas de realizar una importacion de modulos, en esta guia se usarán las principales.

_La importacion la realizamos llamando al elemento especifico y la ruta del el archivo_

```javascript
import {key} from "./key.js"

console.log(key);

//print 27hd7236
```

_Es posible importar varios elementos de un mismo archivo_

```javascript
import {key , user} from "./values.js"

console.log( user , key );

//print 3987987985465 ,  trf44678
```

_Se puede usar un * para traer todo los elementos podemos de un archivo_

```javascript
import * from "./values.js"
```

_Tenemos la opcion de renombrar las importaciones con la palabra **as**_

```javascript
import {getKey , getUser as USER } from "./functions.js"
 
getKey(30);

USER(30);

```

_Cuando se importa desde un modulo exportado por default no va dentro de llames {}, solo el nombre del elemento por default_

```javascript
import User from "./class.js"

const user = new User("Pedro");

user.getName();
```
Si desea ampliar la informacion acerca de _import_ puede ingresar [aqui](https://developer.mozilla.org/es/docs/Web/JavaScript/Referencia/Sentencias/import)

---
# Estructura de archivos

El gran cambio con respecto a _Gulp_ es la modularizacion del codigo, ya que no es necesario modificar la configuracion o el _pipe_ para incluir archivos dentro del flujo de trabajo, pero este cambio presenta un nuevo reto, la estruturacion de nuestro proyecto.

En esta guia se propone una manera de estructurar cualquier aplicacion de Javascript basado en la estructura de carpetas que se han implementado con frameworks como _React_ o _Angular_, tambien integrando dos conceptos basicos en este esquema.

- Un objeto por archivo
- Agrupar archivos relacionados en carpetas

Para ampliar la informacion sobre la estructuracion de aplicaciones en Javascript, puede revisar el capitulo 12 del libro [Maintainable JavaScript](http://www.r-5.org/files/books/computers/languages/escss/style/Nicholas_C_Zakas-Maintainable_JavaScript-EN.pdf)

Se encuentran en comun estas carpetas como elementos fundamentales dentro de una apicacion moderna en javascript. 

- Components
- Modules
- Services
- Clases
- Styles

## Components
Los componentes estan basados en el principio de [Web Components](https://www.humanlevel.com/articulos/desarrollo-web/que-son-y-en-que-consisten-los-web-components.html), son la encapsulacion de un elemento HTML que incluye su propio CSS y JS para funcionar correctamente. Se pueden asociar a elementos concretos de la UI del usuario.

## Modules
Los modulos **NO** son un componente de UI ni necesitan archivos SCSS o CSS para su funcionamiento , basicamente es codigo Javascript separado preferiblemente por funcionalidades.

## Services
Esta carpeta concentra todos los elementos para el consumo de servicios tales como ajax , fetch, axios, encapsulado lo necesario para una comunicacion del lado del cliente.

## Clases
Almacena todas las clases que se definen dentro del proyecto.
Por si existe alguna confusion, Un modulo puede exportar uno o varios elementos, las clases se exportar con la palabra **default** por lo que solo tienen un elemento que se exporta. La clase puede ser instanciada dentro de un modulo , pero no un modulo ser llamado dentro de una clase.
## Styles
Contiene todos los archivos de estilo necesarios en el proyecto.

## Directorio
La carpeta principal seria **src**, esta debe estar en el mismo nivel que el archivo **webpack.config.js**
Dentro de esta carpeta encontraremos encapsulados varios conceptos de una aplicacion moderna con JavaScript.

**esta estructura esta sujeta a cambios**

```
|-- src
    |-- core
        |-- components
          |-- [+] promotion
          |-- modal
              |-- modal.js
              |-- modal.scss
        |
        |-- modules
          |-- [+] localStorage
          |-- sesion
              |-- sesion.js
        |
        |-- services
          |-- [+] authentication
          |-- apiStarWars
              |--starWars.js
        |
        |-- clases
          |-- [+] models
          |-- segment
              |-- segment.js
        |
        |-- styles
              |-- main.scss
        |
        |
        |-- index.js
|
|
|-- webpack.config.js
```
---
