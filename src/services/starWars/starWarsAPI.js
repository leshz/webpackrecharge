import axios from 'axios';

const service = "https://swapi.co/api/";

function getData(datos) { 
    return new Promise((resolve, reject) => {
      axios.get(`${service}${datos}`)
        .then((response) => {
          resolve(response.data);
        })
    })
}

export { getData }