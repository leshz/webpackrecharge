import { getData } from "./services/starWars/starWarsAPI";

const printer = document.getElementById("data");

document.getElementById("fetch").addEventListener("click", async () => {
  let data = await getData("people/8/");
  printer.innerText = JSON.stringify(data, null, "\t");
});